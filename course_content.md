|**Workshop**|**Protein bioinformatics for beginners**|
|----------|:-------------:|------:|
|**Dates**|8 - 9 November|
|**Time**|09:30 - 17:00 hrs|
|**Venue**|ATC Computer lab, EMBL Heidelberg|
|**Trainers**|Toby Gibson, Marc Gouw, Michael Kuhn, Manjeet Kumar, Benjamin Lang, Malvika Sharan|

## List of resources that will be covered in this workshop

**Part-1 Protein databases and sequence analysis**

1. Protein databases:
    - Introduction to protein databases
    - Quick overview of NCBI
    - UniProt - Ben
        - Swissprot and Trembl 
        - Cross-refrences and link-outs
            - OMIM, Domains, GO, ...
2. Study of similar sequences - Marc
    - BLAST
        - BLASTp, BLASTn, PSI-BLAST, ...
    - Diamond
    - HMMER  
    - HHPred
3. Multiple sequence alignments
    - Clustal omega (EMBL-EBI)
    - COBALT (NCBI)
4. Other resources
    - Human Protein Atlas
    - Antibodypedia
    - EMBOSS toolkits
        - EMBOSS dot-plot
        - dotmatcher
        - Pepinfo
        - Sixpack

**Part-2 Protein structure analysis**

*Lecture (Toby):* Secondary vs tertiary structure vs protein complexes

1. Protein Structures: Toby
    - Structure database: PDB
    - Structure visualization
        - Chimera
2. Structure prediction: Malvika
    - jpred: Jalview
    - Tertiary structure prediction: Phyre2, I-TASSER
3. Domain databases: Manjeet
    - InterPro & InterProScan
    - SMART, SUPERFAMILY
    - Pfam
    - SCOP 
4. Prediction of transmembrane helices in proteins: Manjeet
    - TMHMM
    - IUPRED and Anchor
5. Intrinsically disordered region: Marc
    - ELM
    - DISPROT
6. Protein-protein interaction: Michael
    - STRING and STITCH
    - Intact
    - MINT
7. Motif visualization:
     - Weblogo (Malvika)
     - MEME (Malvika)
     - Jalview (Toby)

### Important references
    - http://www.sciencedirect.com/science/book/9788131222973
    - http://molbiol-tools.ca/Protein_Chemistry.htm
    - http://www.ebi.ac.uk/Tools/pfa/

### [Post workshop survey](https://www.surveymonkey.de/r/2GCN32Q)