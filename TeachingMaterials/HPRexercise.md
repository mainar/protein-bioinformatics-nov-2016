# Exploring the Human Protein Atlas

http://www.proteinatlas.org

Antibodies raised against most human proteins.
Rigorous purification protocol.
Used to Stain human tissues and cells. 

Key fact: Provides independent validation of cellular location and tissue distribution using commercial (or home produced) antibodies. 

## Example proteins to explore the Atlas

1. A-kinase anchoring proteins are scaffolds for the PKA kinase.
	- AKAP1
	- AKAP4
	- AKAP8
	- AKAP12

	###Questions
	- Are these AKAPs all found in the same cell compartments and subcellular locations? 
	- What happens when you toggle the channels? 
	- Are these AKAPs found in all tissues? 
	- Are they highly expressed in cancer cells?
	- Is there a PKA kinase cascade?

1. c-Myc transcription factor

	### Questions
	- Do all antibodies perform similarly? (Click for primary data for a summary).
	- In the cerebral cortex are all cell types stained?
	- Would we expect that for Myc? 
	- Which cells are stained in the placenta?
	- Which cell compartments have Myc? 

3. CTNNB1 – the key Wnt signalling transcription factor beta catenin

	### Questions
	- Which tissues don’t express any beta catenin? 
	- Which cancers don’t express any beta catenin? 
	- Is most of the beta catenin staining in the nucleus? 

4. FGF13 - fibroblast growth factor 13

	### Questions
	- Is FGF13 strongly expressed in most cancer cells? 
	- Are there any tissues that don’t stain for FGF13? 
	- Growth factors are secreted and their receptors are on the cell surface: Which cellular compartments contain FGF13? 
	- Which cellular compartments in bronchial tissue contain FGF13? 

5. Try your own favourite proteins! 

Let us know if the images make sense to you…



